<?php 

define (RECIPE_TYPE, 5);

/**
 * Home Service 
 */

class HomeService extends Service
{
  
  public function getPlaceLikeInfo($placeId)
  {
    $placeId = intval($placeId);
    $query = "SELECT
           sum(`item_plugs`.`snootyLikes`) as `totalSnootyLikes`,
           sum(`item_plugs`.`snootyUnlikes`) as `totalSnootyUnlikes`,
           sum(`item_plugs`.`glutieLikes`) as `totalGlutieLikes`,
           sum(`item_plugs`.`glutieUnlikes`) as `totalGlutieUnlikes`
          FROM
          `items`
          INNER JOIN `item_plugs`
          ON (`item_plugs`.`itemId` = `items`.`id`)
          WHERE `items`.`placeId` = '$placeId';";
    $item = $this->db->query($query);
    return $item[0];
  }
  
  public function getItemsFromPlace($placeId, $start, $count)
  {
    
    $query = "select *
              from `items` 
              where (`items`.`plugs` > 0) AND (`placeId` = $placeId) 
              order by `items`.`updateAt` desc 
              limit $start, $count;";
    $items = $this->db->query($query);
    $data = array();
    
    foreach ($items as &$item)
    {
      $data[] = $this->fillItemDetails($item);
    }
    
    return $data;
    
  }
  /**
   * Get latest items 
   */
  public function get($start, $count)
  {
    $query = "select * from `items` where `plugs` > 0 order by `updateAt` desc limit $start, $count;";
    $items = $this->db->query($query);
    $data = array();
    
    foreach ($items as &$item)
    {
      if (intval($item["type"]) == RECIPE_TYPE)
      {
        //echo "Start Fill Recipe Plugs";
        $data[] = $this->fillRecipeDetails($item);
        //echo "OK";
      }
      else 
      {
        //echo "Start Fill Item Plugs";
        $data[] = $this->fillItemDetails(&$item);
        //echo "OK";
      }
    }
    
    return $data;
    
  }
  
  function fillRecipeDetails($recipeItem)
  {
    $userId = $this->user["id"];
    //
    $recipeId = $recipeItem["id"];
    $favouriteItem = $this->db->query("select `id` from `favourites` where `itemId`='$recipeId' and `userId`='$userId' and type='". RECIPE_TYPE ."';");
    
    $recipeItem["favourite"] = sizeof($favouriteItem);
    
    $query = "SELECT `recipe_plugs`.*,  DATE_FORMAT(`recipe_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`, `users`.`firstName`, `users`.`lastName`
              FROM `recipe_plugs` 
              INNER JOIN `users`
              ON (`users`.`id` = `recipe_plugs`.`userId`)
              WHERE `itemId` = '" . $recipeItem["id"] . "'
              ORDER BY `recipe_plugs`.`id` DESC;";
    $plugs = $this -> db -> query($query);
    foreach ($plugs as &$plug) 
    {
      //var_dump("SELECT liked FROM `recipe_plug_likes` WHERE `recipePlugId`={$plug['id']} AND `userId`='${$this->user['id']}';");
      $like = $this -> db -> query("SELECT liked FROM `recipe_plug_likes` WHERE `recipePlugId`='{$plug['id']}' AND `userId`='" . $this -> user["id"] . "';");

      if (sizeof($like)) 
      {
        $plug['like'] = $like[0]['liked'];
      } 
      else 
      {
        $plug['like'] = -1;
      }

    }

    $recipeItem["plugs"] = $plugs;
    return $recipeItem;
  }
  
  function fillItemDetails($item)
  {
    $userId = $this->user["id"];
    
    $place = $this->db->findPlace(intval($item['placeId']));
    //
    $placeId = $place["id"];
    $favouriteItem = $this->db->query("select `id` from `favourites` where `itemId`='$placeId' and `userId`='$userId' and type='" . $place['type'] . "';");
    
    $place["itemId"] = $item["id"];
    $place["type"] = $item["type"];
    $place["itemName"] = $item["name"];
    $place["favourite"] = sizeof($favouriteItem);
      
    $place["plugs"] = array();
      
     
    $query = "SELECT `item_plugs`.*, DATE_FORMAT(`item_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`, `users`.`firstName`, `users`.`lastName`
              FROM `item_plugs` 
              LEFT JOIN `users`
              ON (`users`.`id` = `item_plugs`.`userId`)
              WHERE `itemId` = '". $item["id"] . "'
              ORDER BY `item_plugs`.`id` DESC;";
              
    $plugs = $this->db->query($query);
    
    foreach($plugs as &$plug)
    {
      $like = $this->db->query("SELECT liked FROM `item_plug_likes` WHERE `itemPlugId`={$plug['id']} AND `userId`='" . $this -> user["id"] . "';");
      if ($like)
      {
        $plug['like'] = $like[0]['liked'];
      }
      else 
      {
        $plug['like'] = -1;  
      }    
    }
  
    $place["plugs"] = array_merge($place["plugs"], $plugs);
    return $place;  
  }
  
  
  
} 

?>