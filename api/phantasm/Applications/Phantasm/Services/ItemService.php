<?php

define(RESTAURANT_TYPE, 1);

/**
 * The service handles all operation related to restaurant
 */
class ItemService extends Service {

    /**
     * Search item 
     */
    public function search($query, $start, $count, $type = "1, 2") { // "3, 4"
        $query = "select `places`.*, `items`.`id` as `itemId`
        from `places` 
        inner join `items`
        inner join `item_plugs`
        on (`items`.`placeId` = `places`.`id` AND `item_plugs`.`itemId` = `items`.`id`)
        where `places`.`type` IN ($type)
        and 
        (
          (`places`.`name` LIKE '%$query%')
          or (`items`.`name` LIKE '%$query%')
          or (`item_plugs`.`tags` LIKE '%$query%')
        )  
        group by `items`.`id`
        order by `items`.`updateAt` desc
        limit $start, $count";

        $places = $this->db->query($query);

        return $this->fillItemDetails($places);
    }

    public function getNearest($lat, $long, $distance = 500, $start, $count, $type = "1, 2") {

        /* $query = "SELECT `places`.*, 
          `items`.`id` as `itemId`
          FROM `places`
          INNER JOIN `items`
          ON (`items`.`placeId` = `places`.`id`)
          WHERE (ABS(latitude - $lat)  < $maxX) and (ABS(longitude - $long) < $maxY)
          AND `places`.`type` IN ($type)
          ORDER BY ((latitude - $lat) * (latitude - $lat) + (longitude - $long) * (longitude - $long))
          LIMIT $start, $count;";
         */


        $query = "SELECT  `places` . * ,  `items`.`id` AS  `itemId` , 3956 *2 * ASIN( SQRT( POWER( SIN( ( $lat - ABS(  `places`.`latitude` ) ) * PI( ) /180 /2 ) , 2 ) + COS( $lat * PI( ) /180 ) * COS( ABS( latitude ) * PI( ) /180 ) * POWER( SIN( (
$long -  `places`.`longitude`
) * PI( ) /180 /2 ) , 2 ) ) ) AS distance
FROM  `places` 
INNER JOIN  `items` ON (  `items`.`placeId` =  `places`.`id` ) 
WHERE  `places`.`type` 
IN ( $type ) 
HAVING distance < $distance
ORDER BY distance
LIMIT $start , $count";


        $places = $this->db->query($query);
        return $this->fillItemDetails($places);
    }

    /**
     * 
     */
    public function searchNearest($keyword, $lat, $long, $distance = 500, $start, $count, $type = "1, 2") {

        /* $query = "select 
          `places`.*,
          `items`.`id` as `itemId`
          from `places`
          inner join `items`
          inner join `item_plugs`
          on (`items`.`placeId` = `places`.`id` AND `item_plugs`.`itemId` = `items`.`id`)
          where `places`.`type` IN ($type)
          and
          (
          (`places`.`name` LIKE '%$query%')
          or (`items`.`name` LIKE '%$query%')
          or (`item_plugs`.`tags` LIKE '%$query%')
          )
          and (ABS(latitude - $lat)  < $maxX) and (ABS(longitude - $long) < $maxY)
          AND `places`.`type` IN ($type)

          group by `items`.`id`
          ORDER BY ((latitude - $lat) * (latitude - $lat) + (longitude - $long) * (longitude - $long))
          limit $start, $count"; */

        $query = "SELECT `places` . * , `items`.`id` AS `itemId` , `items`.`name` , `places`.`name` , `item_plugs`.`tags` , 3956 *2 * ASIN( SQRT( POWER( SIN( ( $lat - ABS( `places`.`latitude` ) ) * PI( ) /180 /2 ) , 2 )+ COS( $lat * PI( ) /180 ) * COS( ABS( latitude ) * PI( ) /180 ) * POWER( SIN( (
$long - `places`.`longitude`
) * PI( ) /180 /2 ) , 2 ) ) ) AS distance
FROM `places` 
INNER JOIN `items` 
INNER JOIN `item_plugs` ON ( `items`.`placeId` = `places`.`id` 
AND `item_plugs`.`itemId` = `items`.`id` ) 
WHERE `places`.`type` 
IN ( $type ) 
AND (
(
`places`.`name` LIKE '%$keyword%'
)
OR (
`items`.`name` LIKE '%$keyword%'
)
OR (
`item_plugs`.`tags` LIKE '%$keyword%'
)
)
HAVING distance < $distance
ORDER BY distance
LIMIT 0 , 4";


        $places = $this->db->query($query);
        return $this->fillItemDetails($places);
    }

    /**
     * Add to favourite
     */
    public function addToFavourite($itemId, $type = 1) {
        $userId = intval($this->user["id"]);

        if (!$userId) {
            throw new Exception("UnAuthorized User");
        }

        $favouriteId = $this->db->query("select `id` from `favourites` where `itemId`='$itemId' and `userId`='$userId' and type='$type';");

        if (!sizeof($favouriteId)) {
            $this->db->insertFavourite(array(
                'userId' => $userId,
                'itemId' => $itemId,
                'type' => $type
            ));
        }

        return array("ok" => true);
    }

    public function removeFromFavourite($itemId, $type = 1) {
        $userId = intval($this->user["id"]);

        if (!$userId) {
            throw new Exception("UnAuthorized User");
        }

        $this->db->query("Delete from `favourites` where `itemId`='$itemId' and `userId` = '$userId' and type='$type';");
        return array("ok" => true);
    }

    /**
     * Get Favourite Recipes 
     */
    public function getFavourite($start, $count, $type = '1, 2') {
        $userId = intval($this->user["id"]);
        $query = "SELECT `item_plugs`.*,
    			DATE_FORMAT(`item_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`, 
    			`users`.`firstName`, 
    			`users`.`lastName`
                  FROM 
                  `favourites`
                  INNER JOIN `item_plugs` 
                  ON (`favourites`.`itemId` = `item_plugs`.`id`)
                  INNER JOIN `users`
                  ON (`users`.`id` = `favourites`.`userId`)
                  WHERE (`users`.`id` = '$userId')
                  AND (`favourites`.`type` IN ($type))
                  ORDER BY `favourites`.`id` DESC
                  LIMIT $start, $count;";

        $plugs = $this->db->query($query);
        $places = array();

        foreach ($plugs as &$plug) {
            $query = "SELECT `places`.*, 
	    		`items`.`id` as `itemId`,
	    		`items`.`name` as `itemName`
              FROM `places`
              INNER JOIN `items`
              ON (`items`.`placeId` = `places`.`id`)
              WHERE `places`.`type` IN ($type) 
              AND (`items`.`plugs` > 0)
              AND (`items`.`id` = '{$plug['itemId']}')
              ORDER by `items`.`plugs` DESC";
            $place = $this->db->query($query);

            if (sizeof($place)) {
                $place = $place[0];

                $like = $this->db->query("SELECT liked FROM `item_plug_likes` WHERE `itemPlugId`={$plug['id']} AND `userId`='" . $this->user["id"] . "';");

                if (sizeof($like)) {
                    $plug['like'] = $like[0]['liked'];
                } else {
                    $plug['like'] = -1;
                }

                $plug['favourite'] = 1;

                $place['plugs'] = array($plug);
                $places[] = $place;
            }
        }

        return $places;
    }

    /**
     * Get Favourite Recipes 
     */
    public function getPopular($start, $count, $type = "1, 2") {
        $userId = intval($this->user["id"]);
        $query = "SELECT `places`.*, `items`.`id` as `itemId`
              FROM `places`
              INNER JOIN `items`
              ON (`items`.`placeId` = `places`.`id`)
              WHERE `places`.`type` IN ($type) 
              AND (`items`.`plugs` > 0)
              ORDER by `items`.`plugs` DESC
              LIMIT $start, $count;";
        $places = $this->db->query($query);
        return $this->fillItemDetails($places);
    }

    /**
     * Find all restaurant in specific area
     */
    public function getItemsInArea($lat, $long, $type = "1, 2") {
        $query = "select 
    			`places`.*, 
    			`items`.`id` as `itemId`
        from `places` 
        inner join `items`
        on (`items`.`placeId` = `places`.`id`)
		AND (ABS(latitude - $lat)  < 0.01) and (ABS(longitude - $long) < 0.01)
        AND `places`.`type` IN ($type)
        AND (`items`.`plugs` > 0)
        GROUP BY `items`.`id`
		ORDER BY ((latitude - $lat) * (latitude - $lat) + (longitude - $long) * (longitude - $long));";

        $places = $this->db->query($query);
        return $this->fillItemDetails($places);
    }

    /**
     * List all restaurant
     */
    public function getItems($start = 0, $count = 5, $type = "1, 2") {

        $query = "select `places`.*, `items`.`id` as `itemId`
        from `places` 
        inner join `items`
        on (`items`.`placeId` = `places`.`id`)
        where `places`.`type` IN ($type)
        AND (`items`.`plugs` > 0)
        order by `items`.`updateAt` desc
        limit $start, $count";

        $places = $this->db->query($query);

        return $this->fillItemDetails(&$places);
    }

    /**
     * Fill details of the places
     */
    public function fillItemDetails($places) {
        $userId = $this->user["id"];

        foreach ($places as &$place) {
            //
            $placeId = $place["id"];
            $itemId = intval($place["itemId"]);


            $item = $this->db->findItem($itemId);
            $place["plugs"] = array();

            if ($item) {
                $place["type"] = $item["type"];
                $query = "SELECT `item_plugs`.*, DATE_FORMAT(`item_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`, `users`.`firstName`, `users`.`lastName`
                  FROM `item_plugs` 
                  INNER JOIN `users`
                  ON (`users`.`id` = `item_plugs`.`userId`)
                  WHERE `itemId` = '" . $item["id"] . "'
                  ORDER BY `item_plugs`.`id` DESC;";

                $plugs = $this->db->query($query);

                foreach ($plugs as &$plug) {
                    $like = $this->db->query("SELECT liked FROM `item_plug_likes` WHERE `itemPlugId`={$plug['id']} AND `userId`='" . $this->user["id"] . "';");
                    if (sizeof($like)) {
                        $plug['like'] = $like[0]['liked'];
                    } else {
                        $plug['like'] = -1;
                    }

                    $favouriteItem = $this->db->query("select `id` from `favourites` where `itemId`='{$plug['id']}' and `userId`='$userId' and type='" . $place['type'] . "';");

                    $plug["favourite"] = sizeof($favouriteItem);
                }

                $place["itemName"] = $item["name"];

                $place["plugs"] = array_merge($place["plugs"], $plugs);
            }
        }
        return $places;
    }

    /**
     * Get Items
     */
    public function getItemsFromPlace($placeId, $type) {
        return $this->db->findAllItems(array("placeId" => $placeId, `type` => $type));
    }

    /**
     * Add an item to plug later
     */
    public function addItem($placeId, $itemName, $type = 1) {
        $userId = $this->user["id"];

        if (!$userId) {
            throw new Exception("UnAuthorized User");
        }

        $item = $this->db->findItem(
                array("placeId" => $placeId,
                    "name" => $itemName,
                    "type" => $type));

        if (!$item) {
            $item = array(
                'placeId' => $placeId,
                'name' => $itemName,
                'userId' => $userId,
                'type' => $type
            );

            $item["id"] = $this->db->insertItem($item);
        }

        return array("item" => $item);
    }

    /**
     * add a Restaurant Plug
     */
    public function addItemPlug($itemId, $like, $thought, $tags) {
        $userId = $this->user["id"];

        if (!$userId) {
            throw new Exception("UnAuthorized User");
        }

        $query = "select `type`, `placeId` from `items` where `id`='$itemId';";
        $item = $this->db->query($query);
        //$type = intval($item[0]["type"]);
        $placeId = $item[0]["placeId"];

        $newItemPlug = array(
            "itemId" => $itemId,
            "thought" => $thought,
            "tags" => $tags,
            "userId" => $userId
        );

        $like = intval($like);
        $isSnooty = (intval($this->user["type"]) == 0);

        if (intval($like) != 0) {
            if ($isSnooty) {
                $newItemPlug["snootyLikes"] = 1;
            } else {
                $newItemPlug["glutieLikes"] = 1;
            }
        } else {
            if ($isSnooty) {
                $newItemPlug["snootyUnlikes"] = 1;
            } else {
                $newItemPlug["glutieUnlikes"] = 1;
            }
        }

        $newItemPlugId = $this->db->insertItemPlug($newItemPlug);

        $this->db->insertItemPlugLike(array(
            "itemPlugId" => $newItemPlugId,
            "userId" => $userId,
            "liked" => $like
        ));

        $imageName = "$newItemPlugId.jpg";

        if ($this->uploadFile("itemplugs/$imageName")) {
            $this->db->updateItemPlug(array(
                "id" => $newItemPlugId,
                "image_name" => $imageName
            ));

            $this->db->query("update `items` set `plugs` = `plugs` + 1, `updateAt` = '" . time() . "' where `id`='$itemId';");

            $this->db->query("update `places` set `plugs` = `plugs` + 1 where `id`='$placeId';");

            return array("OK" => true);
        } else {
            return array("Error" => true);
        }
    }

    /**
     * Likes 
     */
    public function likeItemPlug($itemPlugId, $like, $type = 1) {
        $userId = $this->user["id"];

        if (!$userId) {
            throw new Exception("UnAuthorized User");
        }

        $likeItem = $this->db->query("SELECT * FROM `item_plug_likes` WHERE `userId` = '{$this->user["id"]}' AND `itemPlugId` = '$itemPlugId';");
        $itemPlug = $this->db->findItemPlug($itemPlugId);
        $like = intval($like);
        $isSnooty = (intval($this->user["type"]) == 0);

        if (sizeof($likeItem)) {
            $likeItem = $likeItem[0];
            if (intval($likeItem["liked"]) != $like) {
                if ($isSnooty) {
                    if ($like) {
                        $setQuery = "Set snootyLikes = snootyLikes + 1, snootyUnlikes = snootyUnlikes - 1";
                    } else {
                        $setQuery = "Set snootyLikes = snootyLikes - 1, snootyUnlikes = snootyUnlikes + 1";
                    }
                } else {
                    if ($like) {
                        $setQuery = "Set glutieLikes = glutieLikes + 1, glutieUnLikes = glutieUnLikes - 1";
                    } else {
                        $setQuery = "Set glutieLikes = glutieLikes - 1, glutieUnLikes = glutieUnLikes + 1";
                    }
                }

                $this->db->query("update `item_plugs` $setQuery WHERE id=" . $itemPlug["id"]);
                $likeItem['liked'] = $like;
                $this->db->updateItemPlugLike($likeItem);
            } // likes != 
        } else {
            if (intval($like) != 0) {
                if ($isSnooty) {
                    $setQuery = "SET snootyLikes = snootyLikes + 1";
                } else {
                    $setQuery = "SET glutieLikes = glutieLikes + 1";
                }
            } else {
                if ($isSnooty) {
                    $setQuery = "SET snootyUnlikes = snootyUnlikes + 1";
                } else {
                    $setQuery = "SET glutieUnlikes = glutieUnlikes + 1";
                }
            }

            $this->db->query("update `item_plugs` $setQuery WHERE id=" . $itemPlug["id"]);
            $this->db->insertItemPlugLike(array(
                "itemPlugId" => $itemPlugId,
                "userId" => $userId,
                "liked" => $like
            ));
        }

        return array("ok" => true);
    }

}

?>