<?php

/**
 * The service that handles all operation related to 
 * places
 */
class PlaceService extends Service 
{

	// find place foursquare id
	public function findPlace($foursquare_id, $type = 1)
	{
		if ($type == 1 || $type == 2)
	    {
	      $type = "1, 2";
	    }
	    else 
	    {
	      $type = "3, 4";  
	    }
	
	   return $this->db->query("SELECT `name`,`id` FROM `places` 
	                            WHERE `foursquare_id` = '". $foursquare_id . "' AND TYPE in ($type)");
		
	}
	
  /**
   * Add a place
   */
  public function addPlace($latitude, $longitude, $name, $address, $city, $state, $country, $type = 1, $phone, $website, $postalCode)
  {
  
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $place = array(
      'latitude' => $latitude,
      'longitude' => $longitude,
      'name' => $name,
      'address' => $address,
      'city' => $city,
      'state' => $state,
      'country' => $country,
      'type' => $type,
	 'phone' => $phone,
	 'website'=> $website,
   	'postalCode' => $postalCode
    );
    
    $place["id"] = $this->db->insertPlace($place); 
    
    return array("ok" => $place);    
  }  


/**
   * Add a place 2. has foursquare id
   */
  public function addPlace2($latitude, $longitude, $name, $address, $city, $state, $country, $type = 1, $foursquare_id = '', $phone, $website, $postalCode)
  {

    $userId = intval($this->user["id"]);

    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }

    $place = array(
      'latitude' => $latitude,
      'longitude' => $longitude,
      'name' => $name,
      'address' => $address,
      'city' => $city,
      'state' => $state,
      'country' => $country,
      'type' => $type,
   	  'foursquare_id'=> $foursquare_id,
	 'phone' => $phone,
	 'website'=> $website,
	 'postalCode' => $postalCode
    );

    $place["id"] = $this->db->insertPlace($place); 

    return array("ok" => $place);    
  }

  
  /**
   * Return places that are close to give latitude and longitude.
   */
  public function getClosestPlace($lat, $long, $type = 1)
  {
    if ($type == 1 || $type == 2)
    {
      $type = "1, 2";
    }
    else 
    {
      $type = "3, 4";  
    }
    return $this->db->query("SELECT * FROM `places` 
                            WHERE (latitude < $lat + 0.01) AND (latitude > $lat - 0.01) 
                            AND (longitude < $long + 0.01) AND (longitude > $long - 0.01)
                            AND TYPE in ($type)
                            ORDER BY ((latitude - $lat) * (latitude - $lat) + (longitude - $long) * (longitude - $long))");
  }
};

?>