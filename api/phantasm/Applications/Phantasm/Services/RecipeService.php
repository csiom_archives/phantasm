<?php

define(RECIPE_TYPE, 5);

class RecipeService extends Service {

  /**
   * Get Favourite Recipes 
   */
  public function getFavourite($start, $count)
  {
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $query = "SELECT `recipe_plugs`.*,
    			DATE_FORMAT(`recipe_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`,
    			`users`.`firstName`, `users`.`lastName`
    			FROM `favourites`
				inner join `recipe_plugs` 
				ON (`recipe_plugs`.`id` = `favourites`.`itemId`)
				inner join `users` 
				ON (`favourites`.`userId` = `users`.`id`)
				WHERE `favourites`.`type`=" . RECIPE_TYPE . " 
				AND (`favourites`.`userId` = '$userId')
				ORDER by `recipe_plugs`.`id` DESC
				LIMIT $start, $count;";
    $recipePlugs = $this -> db -> query($query);
    $data = array();
    
    foreach ($recipePlugs as &$recipePlug)
    {
	    $recipeId = $recipePlug["itemId"];
	    $recipeItem = $this->db->getItem($recipeId);
	    $recipePlug['favourite'] = 1;
	    
	    $like = $this -> db -> query("SELECT liked FROM `recipe_plug_likes` WHERE `recipePlugId`='{$plug['id']}' AND `userId`='" . $this -> user["id"] . "';");
      
        $recipePlug["favourite"] = 1;

        if (sizeof($like)) 
        {
          $recipePlug['like'] = $like[0]['liked'];
        } 
        else
        {
          $recipePlug['like'] = -1;
        }
	    
	    $recipeItem['plugs'] = array($recipePlug);
	    
	    $data[] = $recipeItem;
    }
    
	

    return $data;
  }
  /**
   * Add to favourite
   */
  public function addToFavourite($recipePlugId)
  {
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    
    $favouriteId = $this->db->query("select `id` from `favourites` where `itemId`='$recipePlugId' and `userId`='$userId' and type='". RECIPE_TYPE ."';");
    
    if (!sizeof($favouriteId))
    {
      $this->db->insertFavourite(array(
        'userId' => $userId,
        'itemId' => $recipePlugId,
        'type' => RECIPE_TYPE
      ));  
    }
    
    return array("ok" => true);
  }
  
  public function removeFromFavourite($recipePlugId)
  {
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $this->db->query("Delete from `favourites` where `itemId`='$recipePlugId' and `userId` = '$userId' and type='". RECIPE_TYPE ."';");
    return array("ok" => true);
  }
  /**
   * Likes
   */
  public function likeRecipePlug($recipePlugId, $like) {
  
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $likeItem = $this -> db -> query("SELECT * FROM `recipe_plug_likes` WHERE `userId` = '{$userId}' AND `recipePlugId` = '$recipePlugId';");
    $recipePlug = $this -> db -> findRecipePlug($recipePlugId);
    $like = intval($like);
    $isSnooty = (intval($this -> user["type"]) == 0);
    if (sizeof($likeItem)) {
      $likeItem = $likeItem[0];
      if (intval($likeItem["liked"]) != $like) {
        if ($isSnooty)
        {
          if ($like)
          {
            $setQuery = "Set snootyLikes = snootyLikes + 1, snootyUnlikes = snootyUnlikes - 1";  
          }
          else 
          {
            $setQuery = "Set snootyLikes = snootyLikes - 1, snootyUnlikes = snootyUnlikes + 1"; 
          }
          
        }
        else
        {
          if ($like)
          {
            $setQuery = "Set glutieLikes = glutieLikes + 1, glutieUnLikes = glutieUnLikes - 1";
          }
          else 
          {
            $setQuery = "Set glutieLikes = glutieLikes - 1, glutieUnLikes = glutieUnLikes + 1"; 
          }  
        } 

        $this->db->query("update `recipe_plugs` $setQuery WHERE id=" . $recipePlugId);
        $likeItem['liked'] = $like;
        $this->db->updateRecipePlugLike($likeItem);

      } // likes !=
    } else {
      if (intval($like) != 0)
      {
        if ($isSnooty)
        {
          $setQuery = "SET snootyLikes = snootyLikes + 1";
        }
        else
        {
          $setQuery = "SET glutieLikes = glutieLikes + 1";  
        }
      }
      else 
      {
        if ($isSnooty)
        {
          $setQuery = "SET snootyUnlikes = snootyUnlikes + 1";
        }
        else
        {
          $setQuery = "SET glutieUnlikes = glutieUnlikes + 1";
        }
      }
      
      $this->db->query("update `recipe_plugs` $setQuery WHERE id=" . $recipePlugId);

      $this -> db -> insertRecipePlugLike(array("recipePlugId" => $recipePlugId, "userId" => $userId, "liked" => $like));
    }

    return array("ok" => true);
  }

  /**
   * Search Recipes
   */
  public function searchRecipes($query, $start, $count) {
    $query = "SELECT `items`.* 
     FROM `items`
     inner join `recipe_plugs`
     ON (`items`.`id` = `recipe_plugs`.`itemId`)
     WHERE `items`.`type`=" . RECIPE_TYPE . " 
     AND ((`items`.`name` LIKE '%$query%') or (`recipe_plugs`.`tags` LIKE '%$query%'))
     GROUP BY `items`.`id` 
     ORDER by `items`.`updateAt` DESC
     LIMIT $start, $count;";
    $recipeItems = $this -> db -> query($query);

    return $this -> fillRecipePlugs($recipeItems);
  }

  /**
   * Get recipes
   */
  public function getRecipes($start, $count) {
    $query = "SELECT * FROM `items` WHERE `type` = " . RECIPE_TYPE . " 
              ORDER BY `items`.`updateAt` desc
              LIMIT $start, $count;";
    $recipeItems = $this -> db -> query($query);

    return $this -> fillRecipePlugs($recipeItems);
  }

  /**
   * Get popular recipes
   */
  public function getPopular($start, $count)
  {
    $query = "SELECT * FROM `items` WHERE `type`='" . RECIPE_TYPE ."'
              ORDER BY `items`.`plugs` DESC
              LIMIT $start, $count;";
    $recipeItems = $this -> db -> query($query);

    return $this -> fillRecipePlugs($recipeItems);
  }
  /**
   * Fill recipe plugs
   */
  public function fillRecipePlugs($recipeItems) {
    $userId = $this->user["id"];
    
    foreach ($recipeItems as &$recipeItem) {
      
      //
      $recipeId = $recipeItem["id"];
            
      $query = "SELECT `recipe_plugs`.*,  DATE_FORMAT(`recipe_plugs`.`timeAdded`, '%b %e, %Y') as `timeAddedFormated`, `users`.`firstName`, `users`.`lastName`
                FROM `recipe_plugs` 
                INNER JOIN `users`
                ON (`users`.`id` = `recipe_plugs`.`userId`)
                WHERE `itemId` = '" . $recipeItem["id"] . "'
                ORDER BY `recipe_plugs`.`id` DESC;";
      $plugs = $this -> db -> query($query);
      foreach ($plugs as &$plug) {
        //var_dump("SELECT liked FROM `recipe_plug_likes` WHERE `recipePlugId`={$plug['id']} AND `userId`='${$this->user['id']}';");
        $like = $this -> db -> query("SELECT liked FROM `recipe_plug_likes` WHERE `recipePlugId`='{$plug['id']}' AND `userId`='" . $this -> user["id"] . "';");

		$favouriteItem = $this->db->query("select `id` from `favourites` where `itemId`='{$plug['id']}' and `userId`='$userId' and type='". RECIPE_TYPE ."';");
      
        $plug["favourite"] = sizeof($favouriteItem);

        if (sizeof($like)) {
          $plug['like'] = $like[0]['liked'];
        } else {
          $plug['like'] = -1;
        }

      }

      $recipeItem["plugs"] = $plugs;
    }
    return $recipeItems;
  }

  /**
   *
   */
  public function getAllRecipes() {
    return $this -> db -> findAllItems(array('type' => RECIPE_TYPE));
  }

  /**
   *
   */
  public function addRecipe($name, $like, $thought, $ingredients, $direction, $tags) {

    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $itemId = $this -> db -> insertItem(array("userId" => $userId, "name" => $name, "type" => RECIPE_TYPE));

    return $this -> addSubRecipe($itemId, $like, $thought, $ingredients, $direction, $tags);
  }

  /**
   * Add a recipe Plugs
   */
  public function addSubRecipe($itemId, $like, $thought, $ingredients, $direction, $tags) {
  
    $userId = intval($this->user["id"]);
    
    if (!$userId) {
      throw new Exception("UnAuthorized User");
    }
    
    $newRecipe = array("itemId" => $itemId, "userId" => $userId, "thought" => $thought, "ingredients" => $ingredients, "direction" => $direction, "tags" => $tags);

    $like = intval($like);
    $isSnooty = (intval($this -> user["type"]) == 0);

    if (intval($like) != 0) {
      if ($isSnooty) {
        $newRecipe["snootyLikes"] = 1;
      } else {
        $newRecipe["glutieLikes"] = 1;
      }
    } else {
      if ($isSnooty) {
        $newRecipe["snootyUnlikes"] = 1;
      } else {
        $newRecipe["glutieUnlikes"] = 1;
      }
    }

    $recipeId = $this -> db -> insertRecipePlug($newRecipe);

    $this -> db -> insertRecipePlugLike(array("recipePlugId" => $recipeId, "userId" => $this -> user["id"], "liked" => $like));
    
    $imageName = "$recipeId.jpg";

    if ($this -> uploadFile("recipeplugs/$imageName")) {
      $this->db->updateRecipePlug(array(
        "id" => $recipeId,
        "image_name" => $imageName
      ));
      
      $this->db->query("update `items` set `plugs`=`plugs` + 1, `updateAt` = '" . time() . "' where `id`='$itemId';");

      return array("OK" => true);
    } else {
      return array("Error" => true);
    }
  }

}
?>

