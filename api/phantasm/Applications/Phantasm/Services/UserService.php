<?php
/**
	* User service handle all operations related to
	* Users.
	*/
class UserService extends Service
{

	public function updateUserType($type)
	{
		if (isset($_SESSION["user"]))
		{
			$user = array(
				"id" => intval($_SESSION["user"]),
				"type" => $type
				);  
			return $this->db->updateUser($user);
		}
		else
		{
			return 0;
		}
	} 

	public function saveInformation($firstName, $lastName, $email, $password, $type)
	{
		if (isset($_SESSION["user"]))
		{
			$user = array(
				"id" => intval($_SESSION["user"]),
				"firstName" => $firstName,
				"lastName" => $lastName,
				"email" => $email,
				"type" => $type
				);  

			if ($password)
			{
				$user["password"] = md5($password);
			}

			return $this->db->updateUser($user);
		}
		else
		{
			return 0;
		}
	}

	/**
		* Get current logged in user
		*/
	public function getUser()
	{
		$user = false;
		if (isset($_SESSION["user"]))
		{
			$user = $this->db->findUser($_SESSION["user"]);	   
		}
		else
		{
			$user = $this->db->findUser(array("sessionId" => session_id()));
			$_SESSION["user"] = $user["id"];
		}

		if ($user)
		{
			return array("user" => $user); 
		}
		else
		{
			return array("error" => "Please login, maybe this account is being used on other device.", "_session" => session_id());
		}
	}

	/**
		* register view
		*/
	public function signUp($email, $password, $username, $type=4)
	{
		if ($this->db->findUser(array('email' => $email)))
		{
			return array('error' => "There's already a registered account with this email address, you can try logging in instead.");
		}
		else
		{
			$user = $this->db->findUser(array('username' => $username, 'password' => sha1($password)));
			if ($user) {
				return array('error' => "There's already a registered account with this email address, you can try logging in instead.");
			}

			$now = new DateTime();
			$date_current = $now->format('Y-m-d H:i:s');	// MySQL datetime format

			$user = array(
				'date_registered' => $date_current,
				'lastlogin' => $date_current,
				'email' => $email,
				'password' => sha1($password),
				'username' => $username,
				'group_id' => $type,
				'lastNoty' => $date_current,
				);

			$id = $this->db->insertUser($user);
			$_SESSION["user"] = $id;
			$user['id'] = $id;
			$this->db->updateUser($user);
			return array('user' => $user, "session" => session_id());
		}
	}


	/**
		* Sign in method 
		* 
		*/
	public function signIn($email, $password)
	{
		$user = $this->db->findUser(array('email' => $email, 'password' => sha1($password)));
		if ($user == NULL) {
			$user = $this->db->findUser(array('username' => $email, 'password' => sha1($password)));
		}
		
		if ($user != NULL)
		{
			$now = new DateTime();
			$date_current = $now->format('Y-m-d H:i:s');	// MySQL datetime format

			$user['lastlogin'] = $date_current;
			$this->db->updateUser($user);

			return array('user' => $user, "session" => session_id());
		}
		else 
		{

			if ($this->db->findUser(array('email' => $email)))
			{
				return array('error' => 'Username or password is not correct.');
			}
			else
			{
				return array('error' => 'Username or password is not correct, you have to sign up first.');		  
			}
		}
	}

	/**
		* Register by facebook
		*/
	public function signUpByFacebook($email, $username, $facebookId, $type=4)
	{
		//--$user = $this->db->findUser(array('email' => $email)); 
		$user = $this->db->findUser(array('fid' => $facebookId)); 
		if ($user)
		{
			return array('error' => "There's already a registered account with this facebook account, please try again with other account.");

		}
		else
		{

			$now = new DateTime();
			$date_current = $now->format('Y-m-d H:i:s');	// MySQL datetime format

			$user = array(
				'lastlogin' => $date_current,
				'email' => $email,
				'password' => sha1($password),
				'username' => $username,
				'group_id' => $type,
				'lastNoty' => $date_current,
				'date_registered' => $date_current,
				'fid' => $facebookId
				);

			$user["id"] = $this->db->insertUser($user);
			$_SESSION["user"] = $user["id"];

			return array('user' => $user, "session" => session_id()); 
		}
	}

	/**
		* Sign in by facebook Id
		* 
		*/
	public function signInByFacebook($facebookId)
	{
		$user = $this->db->findUser(array('fid' => $facebookId));

		if ($facebookId != "" && $user != NULL)
		{
			$now = new DateTime();
			$date_current = $now->format('Y-m-d H:i:s');	// MySQL datetime format
			$user['lastlogin'] = $date_current;
			$this->db->updateUser($user);
			
			return array('user' => $user, "session" => session_id()); 
		}
		else
		{
			return array("error" => "Register with Facebook to login");
		}
	}

	/**
		* Logout of snooty
		*/
	public function signOut()
	{
		unset($_SESSION["user"]);
		return true;
	}


	/**
		* Register by Twitter
		*/
	public function signUpByTwitter($screenName, $twitterId, $type = 0)
	{
		$user = $this->db->findUser(array('twitterId' => $twitterId)); 
		if ($user)
		{
			/* $user['sessionId'] = session_id();
			$this->db->updateUser($user);

			$_SESSION["user"] = $user["id"];
			return array('user' => $user, "session" => session_id());
			*/
			return array('error' => "There's already a registered account with this twitter account, please try again with other account.");

		}
		else
		{
			$user = array(
				'firstName' => $screenName,
				'twitterId' => $twitterId,
				'type' => $type
				);

			$user['sessionId'] = session_id();
			$user["id"] = $this->db->insertUser($user);

			$_SESSION["user"] = $user["id"];
			return array('user' => $user, "session" => session_id()); 
		}
	}

	/**
		* Signin by Twitter
		*/
	public function signInByTwitter($twitterId)
	{
		$user = $this->db->findUser(array('twitterId' => $twitterId)); 
		if ($twitterId != "" && $user)
		{
			$_SESSION["user"] = $user["id"];

			$user['sessionId'] = session_id();
			$this->db->updateUser($user);

			return array('user' => $user, "session" => session_id());
		}
		else
		{
			return array("error" => "This user doesn't exist, you have to sign up first."); 
		}
	}
}

?>
