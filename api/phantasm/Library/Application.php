<?php

require_once 'Database/Database.php';
require_once 'Services/Service.php';
require_once 'Views/h2o/h2o.php';
require_once 'Views/View.php';
require_once 'Views/LoadDataTag.php';
/**
 * The application class that handle
 * an application
 */
class Application
{
  /**
   *
   * @var String name of the application
   */
  public $name;
  
  /**
   *
   * @var Database the database 
   */
  public $db;
  
  /**
   *
   * @var loaded Service
   */
  public $loadedServices = array();


  /**
   * The instance of this class
   */
  private static $instance = NULL;
  
  /**
   * 
   * @param string $name the name of the application
   * @return the application
   */
  public function __construct($name)
  { 
    $this->name = $name;
    
    // Load the database config
    $namePath = realpath(dirname(__FILE__)) . "/../Applications/$name/Configs/Database.php";
    require_once($namePath);


    // Init the database
    $this->db = new Database();
    
    Application::$instance = $this;
     
  }
  
  /**
   * Load a service
   * @param string the service name
   * The requested service
   * @return Service The requested service
   */
  public function loadService($serviceName)
  {
    if (isset($this->loadedServices[$serviceName]))
    {
      return $this->loadedServices[$serviceName];
    }
    else
    {
      $serviceClassName = $serviceName . "Service";
      $servicePath =  realpath(dirname(__FILE__)) . "/../Applications/$this->name/Services/$serviceClassName.php";

      if (file_exists($servicePath))
      {
        require_once $servicePath;
        if (class_exists($serviceClassName))
        {
          $service = new $serviceClassName();
          $service->name = $serviceName;
          $service->db = $this->db;

          $this->loadedServices[$serviceName] = $service;
          return $service;
        }
        else
        {
          throw new Exception("Service '$serviceName' couldn't be loaded!!!");
        }
      }
      else
      {
        throw new Exception("Service '$serviceName' couldn't be found!!!");
      }
    }
  }
  
  
  /**
   * Load a view from view name
   * @param type $viewName
   */
  public function renderView($service, $data)
  {
    // Load the views configs
    require_once realpath(dirname(__FILE__)) . "/../Applications/$this->name/Configs/View.php";
    $viewFile = realpath(dirname(__FILE__)) . "/../Applications/$this->name/Views/$service->view.html";
    
    if (file_exists($viewFile))
    {
      $h2o = new h2o($viewFile);
      echo $h2o->render(array("data" => $data, "service" => $service));
    }
    else
    {
      throw new Exception("View not found '$viewFile'");
    }
    
  }
  
  /**
   * Get the application name
   */
  public function getName()
  {
    return $this->name;
  }
  
  /**
   * 
   * @return Application the instance of Application class
   */
  public static function getInstance()
  {
    return Application::$instance;
  }
};

?>