<?php

/**
 * The database
 */
class Database
{
  /**
   *
   * @var mixed the database configuration
   */
  public static $config;
  
  
  /**
   *
   * @var mixed the connection id
   */
  private $conn;


  /**
   * Create a database connection to the server
   */
  public function __construct() 
  {
    $this->conn = mysql_connect(Database::$config["host"], Database::$config["username"], Database::$config["password"]);
    if ($this->conn) 
    {
      if (!mysql_select_db(Database::$config["database"], $this->conn)) 
      {
        throw new Exception("Couldn't connect database!!!");
      }
    }
    else
    {
      throw new Exception("Couldn't connect to server MySQL!!!");
    }
  }
  
  /**
   * Get an object
   * @param type $id
   * @param type $modelName
   */
  public function find($object = array(), $modelName = null)
  {
    if (!is_array($object))
    {
      $object = array('id' => intval($object));
    }
    
    $result = $this->findAll($object, 0, 1, $modelName . "s");
    return sizeof($result) ? $result[0] : null;
  }
  
  /**
   * Find all row in a table
   * @param type $object
   * @param type $modelName
   */
  public function findAll($object = array(), $first = 0, $count = 0, $modelName = null)
  {
    if (is_int($object))
    {
      $object = array("id" => $object);
    }
    
    if (is_object($object))
    {
      if (!$modelName)
      {
        $modelName = get_class($object);
      }
      
      $object = get_object_vars($object);
    }
    
    if (is_string($first) && !$modelName)
    {
      $modelName = $first;
    }
    
    $tableName = $this->tableName($modelName);
    
    $values = array();
    
    foreach ($object as $key => $value) 
    {
      $values[] = "`$key` = '" . mysql_escape_string($value) . "'";
    }
    
    $condition = "";
    $limit = "";
    
    if (sizeof($values))
    {
      $condition = "WHERE " . implode(" AND ", $values);
    }
    
    if ($count > 0)
    {
      $limit = "LIMIT $first, $count ";
    }
    
    $query = "SELECT * FROM `$tableName` $condition $limit;";
    
    return $this->query($query);
  }


  /**
   * Delete a row from database
   * @param type $id
   * @param type $modelName
   */
  public function delete($object = array(), $modelName = null)
  {
    $id = $object;
    
    if (is_object($object))
    {
      if (!$modelName)
      {
        $modelName = get_class($object);
      }
      
      $id = $object->id;
    }
    
    if (is_array($object))
    {
      $id = $object["id"];
    }
    
    $tableName = $this->tableName($modelName);
    
    return $this->execute("DELETE FROM `$tableName` WHERE `id` = '$id';");
    
  }
  
  /**
   * Update an object in the database
   * @param type $object
   * @param type $modelName
   */
  public function update($object = array(), $modelName = null)
  {
    if (is_object($object))
    {
      if (!$modelName)
      {
        $modelName = get_class($object);
      }
      
      $object = get_object_vars($object);
    }
    
    $tableName = $this->tableName($modelName);
    
    $values = array();
    
    foreach ($object as $key => $value) 
    {
      if ($key == "id")
      {
        $id = $value;
      }
      else
      {
        $values[] = "`$key` = '" . mysql_escape_string($value) . "'";
      }
    }
    
    $query = "UPDATE `$tableName`".
             "SET " . implode(",", $values) .
             " WHERE `id` = '$id';";
    
    $this->execute($query);
    
    return mysql_affected_rows($this->conn);
  }
  
  
  /**
   * Insert an object
   * @param type $object
   */
  public function insert($object = array(), $modelName = null) 
  {
    
    if (is_object($object))
    {
      if (!$modelName)
      {
        $modelName = get_class($object);
      }
      
      $object = get_object_vars($object);
    }
    
    $tableName = $this->tableName($modelName);
    
    $keys = array();
    $values = array();
    
    foreach ($object as $key => $value) 
    {
      $keys[] = "`$key`";
      $values[] = "'" . mysql_escape_string($value) . "'";
    }
    
    $query = "INSERT INTO `$tableName`(" . implode(",", $keys) . ")".
             "VALUES( " . implode(",", $values) . ");";
             
    $this->execute($query);
    
    return mysql_insert_id($this->conn);
         
  }

  /**
   * 
   * @param type $query
   * @return mixed
   */
  public function query($query) 
  {
    $result = array();
    
    $query = str_replace("##", Database::$config["prefix"], $query);
    
    $resource = mysql_query($query, $this->conn) or die(mysql_error($this->conn));
    
    $item = mysql_fetch_assoc($resource);
    
    while ($item) 
    {
      $result[] = $item;
      $item = mysql_fetch_assoc($resource);
    }
    
    return $result;
  }
  
  
  /**
   * Execute a query
   * @param type $query
   * @return int number of affected rows
   */
  public function execute($query)
  {
    $query = str_replace("##", Database::$config["prefix"], $query);
    mysql_query($query, $this->conn) or die ("Error " . mysql_error() . " in query '$query'");
    return mysql_affected_rows();
  }
  
  /**
   * Call object
   * 
   * @param type $name
   * @param type $arguments
   */
  public function __call($name, $arguments) {
    
    $methodName = substr($name, 0, 6);
    
    if ($methodName == "update" || $methodName == "insert" || $methodName == "delete")
    {
      $modelName = substr($name, 6);
    }
    elseif ($methodName == "findAl")
    {
      $methodName = "findAll";
      $modelName = substr($name, 7);
    }
    else if (substr($name, 0, 4) == "find")
    {
      $methodName = "find";
      $modelName = substr($name, 4);
    }
    else if (substr($name, 0, 3) == "get")
    {
      $methodName = "find";
      $modelName = substr($name, 3);
    }
    else
    {
      $methodName = null;
    }
    
    if ($methodName)
    {
      if (!sizeof($arguments))
      {
        $arguments[] = array();
      }
      
      $arguments[] = $modelName;
      return call_user_func_array(array($this, $methodName), $arguments);
    }
  }
  
  /**
   * get the table name
   * @param type $modelName
   * @return type
   */
  public function tableName($modelName) 
  {
    $modelName[0] = strtolower($modelName[0]);
    if (substr($modelName, -1) != "s")
    {
      $modelName .= "s";
    }
    
    return Database::$config["prefix"] . strtolower(preg_replace("/([A-Z])/", "_$1", $modelName));
  }
};

?>
