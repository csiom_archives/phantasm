<?php
require_once(realpath(dirname(__FILE__) . "/../Images/php_image_magician.php"));

/**
 * Image width
 */
define (IMAGE_WIDTH, 540);

/**
 * Class service to handle 
 * a request 
 **/
class Service
{
	/**
	 * @var Database the database 
	 **/
	public $db;
	
	/**
	 * @var String
	 * The service name
	 **/
	public $name;
	
	/**
	 * @var String
	 * The requested action
	 **/
	public $action;
  
  /**
   *
   * @var string
   * The rendered view
   */
  public $view;
	
	/**
	 * @var mixed
	 * The requested parameters
	 **/
	public $params;
	
	/**
	 * The default constructor
	 **/
	public function __construct()
	{
	}
	
	/**
	 * Call before execute the request
	 * Used to filter login
	 **/
	public function beforeExecute()
	{
	}
	
	/**
	 * Call after execute
	 **/
	public function afterExecute()
	{
	}
	
	
	/**
	 * Handle the requested action
	 * @param String action the action name
	 * @param mixed params the attached parameters
	 * return mixed data
	 **/
	public function execute($action, $params)
	{
	    $this->action = $action;
	    $this->params = $params;
	    $this->view = $this->name . "/" . $this->action;
	    
			if (method_exists($this, $action))
	    {
	      // Call before execute filter
	      $this->getCurrentUser(); 
	      $this->BeforeExecute();
	
	      
	      $arguments = array();
	      
	      if (isset($params["arguments"]))
	      {
	        $arguments = $params["arguments"];
	      }
	      
	      if (isset($params["json_arguments"]))
	      {
	          $arguments = json_decode($params["json_arguments"]);
	      }
	      
	      $result = call_user_func_array(array($this, $action), $arguments);
	      
	      // Call after execute processing
	      $this->AfterExecute();
	    }
	    else
	    {
	      throw new Exception("Couldn't find action in service $this->name!!!");
	    }
	    
	    return $result;
	}
  
  /**
   * Move Upload Files to target folder
   */
  public function uploadFile($destination, $file = 'file')
  {
    $fileName = IMAGE_FOLDER . "/" . $destination;
    $result = move_uploaded_file($_FILES[$file]['tmp_name'], $fileName);
    
    if ($result)
    {
      $image = imagecreatefromjpeg($fileName);
      $width = imagesx($image);
      $height = imagesy($image);
      
      $newWidth = IMAGE_WIDTH;
      $newHeight = IMAGE_WIDTH;
      $left = 0;
      $top = 0;
      
      if ($width < IMAGE_WIDTH && $height < IMAGE_HEIGHT)
      {
        imagedestroy($image);
        copy($fileName, $fileName . "thumb.jpg");
        return $result;
      }
      
      if ($width < IMAGE_WIDTH)
      {
        $newWidth = $newHeight = $width;
      }
      
      if ($height < IMAGE_WIDTH)
      {
        $newWidth = $newHeight = $height;
      }
      
      
      $ratioX = $newWidth / $width;
      $ratioY = $newHeight / $height;
      if ($width > $height) 
      {
        $left = intval($width - $height) / 2;
      }
      else 
      {
        $top = intval($height - $width) / 2;
      }
      
      $newImage = imagecreatetruecolor($newWidth, $newHeight);
      imagecopyresampled($newImage, $image,
                          0, 0, 
                          $left, $top,
                          $newWidth, $newHeight, 
                          $width - 2 * $left, $height - 2 * $top
                          );
      imagejpeg($newImage, $fileName . "thumb.jpg", 75);
      imagedestroy($image);
      imagedestroy($newImage);
      
      
    }
    
    return $result;
  }
  
  /**
   * Return current user
   */
  public function getCurrentUser()
  {
    $this->user = NULL;
    if (isset($_SESSION["user"]))
    {
      $this->user = $this->db->findUser(intval($_SESSION["user"]));
    }
    
    return $this->user;
  }
};

?>