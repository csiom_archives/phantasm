<?php

/**
 * Load Data Tag
 * Load data from a service and function and pass to a variable
 */
class Load_Data_Tag extends H2o_Node
{
  // The params
  private $params;

  // 
  private $serviceName;
  
  private $actionName;
  
  private $variableName;
  
  private $paramsList;
  
  /**
   * 
   * @param type $stringargs
   * @param type $parser
   * @param type $position
   */
  public function __construct($stringargs, $parser, $position)
  {
    $matches = array();
    
    preg_match("/^(?P<var>[^\\|]+).(?P<service>[^\\.]+).(?P<action>[^\\(]+).(?P<params>[^\\)]*).$/", $stringargs, $matches);
    
    $this->variableName = trim($matches["var"]);
    $this->serviceName = trim($matches["service"]);
    $this->actionName = trim($matches["action"]);
    $this->params = explode(",", $matches["params"]);
    
  }
  
  /**
   * 
   * @param H2o_Context $context
   * @param type $stream
   */
  public function render($context, $stream)
  {
    $application = Application::getInstance();
    // @var Service $service
    $service = $application->loadService($this->serviceName);
    $args = array();
    
    foreach ($this->params as $item)
    {
      $item = trim($item);
      
      if ($item[0] == "$") 
      {
        $args[] = $context->getVariable(substr($item, 1));
      }
      else
      {
        $args[] = $item;
      }
    }
    
    $params = $_REQUEST;
    $params['arguments'] = $args;
    
    $data = $service->execute($this->actionName, $params);
    $context->set($this->variableName, $data);
  }
}

H2o::addTag("load_data");
?>
