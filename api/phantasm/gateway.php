<?php

try
{
  if (isset($_REQUEST["session_id"]))
  {
    session_id($_REQUEST["session_id"]);
    session_start();
  }
  else 
  {
    session_start();  
  }
  
  require_once 'Library/Application.php';

  $applicationName = isset($_REQUEST["app"]) ? $_REQUEST["app"] : "Maps";
  $serviceName = isset($_REQUEST["service"]) ? $_REQUEST["service"] : "Default";
  $action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "home";
  
  // Load the application
  $application = new Application($applicationName);
  $service = $application->loadService($serviceName);
  $data = $service->execute($action, $_REQUEST);
  
  if (isset($_REQUEST["json"]))
  {
    echo json_encode(
        array(
          "ok" => 1,
          "data" => $data,
          "action" => $action
        )
      );
  }
  else
  {
    // load the view
    $application->renderView($service, $data);
  }

}
catch(Exception $e)
{
  if (isset($_REQUEST["json"]))
  {
    echo json_encode(
        array(
          "failed" => 0,
          "error" => $e->getMessage(),
          "action" => $action
        )
      );
  }
  else
  {
    // load the view
    die("Error: " . $e->getMessage());
  }
}

?>