      'use strict';
      var wavesurfer = Object.create(WaveSurfer);
      var s,e,resume;
      var skip_time  = 0;

      // Function to be run after complete upload process
      function  audioWaves(loadFile) {
      var options = {
        container     : document.querySelector('#waveform'),
        waveColor     : 'green',
        progressColor : 'green',
        cursorColor   : 'navy',
        fillparent    : 'true',
        cursorWidth   : 1,
        minPxPerSec   : 2,
        height        : 64, 
        pixelRatio    : 1,
        hideScrollbar : true 
    };
    
    if (location.search.match('scroll')) {
        options.minPxPerSec = 100;
    }
    // Init
    wavesurfer.init(options);
    // Load audio from URL
    wavesurfer.loadBlob(loadFile);
}

function displayTime(position, seconds)
{
// For future use
//var numdays = Math.floor(seconds / 86400);
//var numhours = Math.floor((seconds % 86400) / 3600);
var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
var numseconds = (((seconds % 86400) % 3600) % 60).toFixed(2);

if (position == "s") {
//document.getElementById("startm").value = numminutes;
//document.getElementById("starts").value = numseconds;
document.getElementById("startm").innerHTML = numminutes;
document.getElementById("starts").innerHTML = numseconds;
document.getElementById("startTime").value = seconds;
}
else {
//document.getElementById("endm").value = numminutes;
//document.getElementById("ends").value = numseconds;
document.getElementById("endm").innerHTML = numminutes;
document.getElementById("ends").innerHTML = numseconds;
document.getElementById("trimDuration").value = seconds - s;
} 
}


wavesurfer.on('ready', function () {
   document.getElementById("trim").disabled=false;
   document.getElementById("trim").className += " btn-info";
   $('#myModal').modal('show');
   //wavesurfer.play();
   s = parseInt(wavesurfer.getDuration()/3);
   e = parseInt(wavesurfer.getDuration() * 2/3);
   console.log(s);
   console.log(e);
   wavesurfer.addRegion({
               id: '1',
            start: s,
              end: e,
            color: 'rgba(0, 0, 255, 0.2)',
             drag: false
      });
   displayTime('s',s);
   displayTime('e',e); 
   
   var timeline = Object.create(WaveSurfer.Timeline);
   timeline.init({
           wavesurfer: wavesurfer,
           container: "#wave-timeline"
      });
});

    wavesurfer.on('seek', function(progress) {
    var duration = wavesurfer.getDuration();
    var seek_time = duration * progress;
    console.log(seek_time);
    if (!wavesurfer.backend.isPaused()) {
    if (seek_time > s && seek_time < e)
       wavesurfer.play(seek_time,e);
    else 
       wavesurfer.play(s,e);
    }
    else {
    if (seek_time > s && seek_time < e)
    resume = seek_time;
    else
    resume = s;
    }
   });

wavesurfer.on('region-updated', function (region) {
    s = region.start;
    e = region.end;
    displayTime('s',s);
    displayTime('e',e);
    resume = undefined;
});

function customPlayPause() {
if (!wavesurfer.backend.isPaused()) {
   wavesurfer.pause(); 
   resume = wavesurfer.getCurrentTime();
}

else {
    if (typeof resume === 'undefined')
       wavesurfer.play(s,e);
    else
       wavesurfer.play(resume,e);
    resume = undefined;
 }
}

//Trim button Behaviour
function toggleTrim() {
if ( document.getElementById("trim").innerHTML == "Trim" ){
    document.getElementById("trim").innerHTML = "Cancel";
    document.getElementById("isFullAudio").value = 0;
    document.getElementById("trim").className = "btn btn-large btn-danger";
    document.getElementById("Subtn").innerHTML = "Save selected";
}
else {
    document.getElementById("trim").innerHTML = "Trim";
    document.getElementById("isFullAudio").value = 1;
    document.getElementById("trim").className = "btn btn-large btn-info";
    document.getElementById("Subtn").innerHTML = "Save Full Audio"; 
}}
