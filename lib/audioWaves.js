      'use strict';
      var wavesurfer = Object.create(WaveSurfer);
      var s,e;
      var sliderInitiated = false;
      // Function to be run after complete upload process
      function  audioWaves(loadFile) {
      var options = {
        container     : document.querySelector('#waveform'),
        waveColor     : 'grey',
        progressColor : 'grey',
        cursorColor   : 'navy',
        fillparent    : 'true',
        cursorWidth   : 1,
        minPxPerSec   : 2,
        pixelRatio    : 1,
        hideScrollbar : true
    };
    
    if (location.search.match('scroll')) {
        options.minPxPerSec = 100;
    }
    // Init
    wavesurfer.init(options);
    // Load audio from URL
    wavesurfer.loadBlob(loadFile);

    var slider = document.getElementById('slider');
    var hr = document.getElementById('below-slider');
 

    if (Modernizr.touch) {
    alert("Touch Device");     
    console.log(slider);
    console.log(below-slider);
    }
    else {
    alert('No touch device');
    slider.style.display='none';
    hr.style.display='none';
    }

}

function displayTime(position, seconds)
{
// For future use
var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
var numseconds = (((seconds % 86400) % 3600) % 60).toFixed(2);

if (position == "s") {
document.getElementById("startm").innerHTML = numminutes;
document.getElementById("starts").innerHTML = numseconds;
document.getElementById("startTime").value = seconds;
}
else {
document.getElementById("endm").innerHTML = numminutes;
document.getElementById("ends").innerHTML = numseconds;
document.getElementById("trimDuration").value = seconds - s;
} 
}


wavesurfer.on('ready', function () {
   $('#myModal').modal('show');
   s = 0;
   e = wavesurfer.getDuration();
   console.log(s);
   console.log(e);
   wavesurfer.addRegion({
               id: '1',
            start: s,
              end: e,
            color: 'rgba(255, 0,0, 0.2)',
             loop: true
      });
   displayTime('s',s);
   displayTime('e',e); 
   
   var timeline = Object.create(WaveSurfer.Timeline);
   timeline.init({
           wavesurfer: wavesurfer,
           container: "#wave-timeline"
   });

   if (Modernizr.touch) {

    noUiSlider.create(slider, {
        start: [s, e],
        connect: true,
        range: {
                'min': 0,
                'max': wavesurfer.getDuration()
        }
    });

    var  sliderEnd  = wavesurfer.getDuration();
   
    slider.noUiSlider.on('update', function( values, handle ) {
        if ( handle ) { console.log('end');
                displayTime('e',values[handle]);
                sliderInitiated = true;
                wavesurfer.regions.list[1].update({end:values[handle]});
                sliderEnd = values[handle];
        } else {  console.log('start');
                  displayTime('s',values[handle]);
                sliderInitiated = true;
                if ( sliderEnd == e.toFixed(2)) {
                wavesurfer.clearRegions();
                   wavesurfer.addRegion({
                   id: '1',
                   start: values[handle],
                   end: sliderEnd,
                   color: 'rgba(0, 0, 255, 0.2)',
                   loop: true
                });
                }
                else
                wavesurfer.regions.list[1].update({start:values[handle]});
        }
});
}
});


wavesurfer.on('region-created',function(region) {
    s = region.start;
    var duration = wavesurfer.getDuration();
    var progress = s/duration;

    wavesurfer.seekTo(progress);
    wavesurfer.regions.wavesurfer.play();
});


wavesurfer.on('seek', function(progress) {
    var duration = wavesurfer.getDuration();
    var newProgress = s/duration;
      if (progress != newProgress) {
       wavesurfer.seekTo(newProgress);
       wavesurfer.regions.wavesurfer.play();
    }
   });


wavesurfer.on('region-click', function (region, e) {
        e.stopPropagation();
        // Play on click, loop on shift click
        //e.shiftKey ? region.playLoop() : region.play();
        wavesurfer.regions.wavesurfer.play();

   });

wavesurfer.on('region-updated', function (region) {
    s = region.start;
    e = region.end;

if (Modernizr.touch) {

    if(sliderInitiated == true)
    sliderInitiated = false;
    else
    slider.noUiSlider.set([s,e]);
}
    displayTime('s',s);
    displayTime('e',e);
    var duration = wavesurfer.getDuration();
    var progress = s/duration;
    wavesurfer.seekTo(progress);
    wavesurfer.regions.wavesurfer.play();
   });

wavesurfer.on('finish', function (){
wavesurfer.regions.wavesurfer.play();

});

function customPlayPause() {
if (!wavesurfer.backend.isPaused()) {
    wavesurfer.regions.wavesurfer.pause();
}
else {
    wavesurfer.regions.wavesurfer.play();
}
}
