<?php include_once('vine.class.php');
$vine = new Vine(get_option('vineusr'), get_option('vinepass'));
$importer = array_merge($_GET, $_POST);
$importer['auto'] = false;
$importer['allowduplicates'] = false;


if(isset($_POST['checkRow'])) {
foreach ($_POST['checkRow'] as $raw) {
$video = base64_decode($raw);
vine_import(maybe_unserialize($video), $importer['categ'],$importer['owner']);
}
echo '<div class="msg-info">Selected videos have been imported.</div>';
}
?>
<?php if (isset($importer['action'])) {

switch ($importer['action']) {
case 'tag':
$vines = $vine->getVinesbyTagJSON($importer['key'], this_page());
$pagi_url = admin_url("vine-1by1").'&action=tag&key='.$importer['key'].'&categ='.$importer['categ'].'&owner='.$importer['owner'].'&p=';
break;
default:
$vines = $vine->getVinesbyPopular(this_page());
$pagi_url = admin_url("vine-1by1").'&action=popular&categ='.$importer['categ'].'&owner='.$importer['owner'].'&p=';
break;
}
if($vines["success"]) {
$nbTotal = isset($vines["total"]) ? $vines["total"] : 0;
//$nbTotal = $nbTotal - 1; /* Fix for the extra blank page */
$nb_display = 25;
if(isset($vines) && ($nbTotal > 0) && isset($vines["returned"]) && ($vines["returned"] > 0)) {

$a = new pagination;	
$a->set_current(this_page());
$a->set_first_page(true);
$a->set_pages_items(12);
$a->set_per_page($nb_display);
$a->set_values($nbTotal);
$a->show_pages($pagi_url);

?>

<div class="row-fluid" style="padding: 10px 0">
</div>
<form id="validate" class="form-horizontal styled" action="<?php echo $pagi_url.this_page();?>" enctype="multipart/form-data" method="post">
<input type="hidden" name="categ" class="hide" value="<?php echo $importer['categ']; ?>"> 
<div class="table-overflow top10">
                        <table class="table table-bordered table-checks">
                          <thead>
                                <tr> 
                                  <th><input type="checkbox" name="checkRows" class="styled check-all" /></th>                                
                                  <th width="130px">Picture</th>								 
                                  <th width="220px">Vine</th>
								  <th>NSFW</th>
								   <th>Created</th>
							      <th>Status</th>								  
                             <th><button class="btn btn-large btn-success" type="submit"><?php echo _lang("Import selected"); ?></button></th>
								</tr>
                          </thead>
                          <tbody>
						  <?php foreach ($vines["videos"] as $vine) { 
						  						  
						  ?>
                              <tr>
                                <td><input type="checkbox" name="checkRow[]" value="<?php echo base64_encode(maybe_serialize(vineobj($vine))); ?>" class="styled" /></td>
                                  <td><img src="<?php echo $vine->thumbnailUrl; ?>" style="width:130px; height:90px;"></td>
                                  <td ><?php echo _html($vine->description); ?></td>
								  <td>
								  <?php
                                  
								  echo intval($vine->explicitContent) ;?>
								  </td>
								  <td>
								  <?php
                                  $dt =explode('T',$vine->created);
								  $ds = explode('.',$dt[1]);
								  $df = $dt[0]. ''.$ds[0];
								  echo time_ago($df) ;?>
								  </td>
                                  <td>
								    <?php if(has_vine_duplicate($vine->permalinkUrl)) {
								    echo '<span class="redText">Warning: Already saved.</span>';
								   } else {
								    echo '<span class="greenText">Unique</span>';								
								   }

                                   ?>
								  </td>
								  <td><a class="btn btn-primary" href="<?php echo $vine->permalinkUrl; ?>" target="_blank"><i class="icon-link"></i>@Vine.co</a></td>
                                  
                              </tr>
							  <?php 
							
							  }  //end loop 
							  ?>
						</tbody>  
</table>
</form>
</div>						
<?php
$next = this_page() + 1;
$a->show_pages($pagi_url);
} else {
$error = isset($vines["error"]) ? $vines["error"] : "No more results from Vine.co";
echo '<div class="msg-warning">'.$error.'</div>';
}
} else {
echo '<div class="msg-warning">Something went wrong</div>';
}


} ?>
<?php if (!isset($importer['action'])) { ?>
<h2 class=""> Vine.co "1 by 1" importer</h2>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#search">Import by #Tag</a></li>
  <li><a href="#popular">Popular Vines</a></li>
   
</ul>

<div class="tab-content" style="min-height:900px">
  <div class="tab-pane active" id="search">
  <div class="row-fluid">
<form id="validate" class="form-horizontal styled" action="<?php echo admin_url('vine-1by1');?>" enctype="multipart/form-data" method="post">

<input type="hidden" name="action" class="hide" value="tag"> 
<div class="control-group">
<label class="control-label"><i class="icon-search"></i>Tag</label>
<div class="controls">
<input type="text" name="key" class="validate[required] span8" value=""> 						
</div>	
</div>

<?php
echo '<div class="control-group">
	<label class="control-label">'._lang("Category:").'</label>
	<div class="controls">
	'.cats_select("categ","select","").'
	  </div>             
	  </div>';
?>	  
	<div class="control-group">
	<label class="control-label">User</label>
	<div class="controls">
	<?php
	echo '<select data-placeholder="'._lang("Choose owner:").'" name="owner" id="clear-results" class="select validate[required]" tabindex="2">
	';
$users = $db->get_results("SELECT id, name FROM  ".DB_PREFIX."users order by id asc limit 0,1000");
if($users) {
foreach ($users as $cat) {	
echo'<option value="'.intval($cat->id).'">'.stripslashes($cat->name).'</option>';
	}
}	else {
echo'<option value="">'._lang("No users").'</option>';
}
echo '</select>';
	
	?>
	</div>
	</div>

		
<div class="control-group">
<button type="submit" class="pull-right btn btn-success">Start import</button> 						

</div>	  
	</form>    
    </div>
   </div> 
  <div class="tab-pane" id="popular">
  <div class="row-fluid">
<form id="validate" class="form-horizontal styled" action="<?php echo admin_url('vine-1by1');?>" enctype="multipart/form-data" method="post">

<input type="hidden" name="action" class="hide" value="popular"> 

<?php
echo '<div class="control-group">
	<label class="control-label">'._lang("Category:").'</label>
	<div class="controls">
	'.cats_select("categ","select","").'
	  </div>             
	  </div>';
?>	  
	<div class="control-group">
	<label class="control-label">User</label>
	<div class="controls">
	<?php
	echo '<select data-placeholder="'._lang("Choose owner:").'" name="owner" id="clear-results" class="select validate[required]" tabindex="2">
	';
$users = $db->get_results("SELECT id, name FROM  ".DB_PREFIX."users order by id asc limit 0,1000");
if($users) {
foreach ($users as $cat) {	
echo'<option value="'.intval($cat->id).'">'.stripslashes($cat->name).'</option>';
	}
}	else {
echo'<option value="">'._lang("No users").'</option>';
}
echo '</select>';
	
	?>
	</div>
	</div>

		
<div class="control-group">
<button type="submit" class="pull-right btn btn-success">Start import</button> 						

</div>	  
	</form>    
    </div>
   </div> 
  </div> 
   
   <?php } ?>