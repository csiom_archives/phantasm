<?php /* Vine class and helper functions */
function has_vine_duplicate($id){
global $db;
$v_id = 'vine.co/v/'.$id;
$sub = $db->get_row("Select count(*) as nr from ".DB_PREFIX."videos where source  like '%".$v_id."'");
return (bool)$sub->nr;
}
function vine_import($video=array(), $cat = null, $owner = null) {
global $db;
if(is_null($owner)) {$owner = get_option('importuser','1');}
if(isset($video["postId"]) && isset($video["thumbnailUrl"]) ) {
/* remove trash from thumb and mp4 */
$videot = explode('?versionId',$video["thumbnailUrl"]);
$video["thumbnailUrl"] = $videot[0];
$videomt = explode('?versionId',$video["videoUrl"]);
$video["videoUrl"] = $videomt[0];
$video["path"] = $video["permalinkUrl"];
/* Add mp4 only if you asked for it */
$remote = '';
if(get_option('vinemode','0') > 0) { $remote = $video["videoUrl"]; }

if ($remote != '') {

$target_path = '/home/phantasm/public_html/rawmedia/';
$final_path = '/home/phantasm/public_html/media/';

$input = $target_path.$token.'.mp4';
$outputVideo = $final_path.$token."1.mp4";
$outputAudio = $final_path.$token."1.aac";
$thumbstore = $final_path.'thumbs/'.$token.'.jpg';

$cmd = "wget $remote -O $input";
exec("$cmd 2>&1");

$cmd0 = "wget ".$video["thumbnailUrl"]." -O $thumbstore";
exec("$cmd0 2>&1");

$cmd1 = get_option('ffmpeg-cmd','ffmpeg')." -i $input -vcodec copy -an $outputVideo";
exec("$cmd1 2>&1");

$cmd2 = get_option('ffmpeg-cmd','ffmpeg')." -i $input -codec:a libfaac -b:a 148000 -r 481000 $outputAudio";
exec("$cmd2 2>&1");

$saveFile = $token.'1';

// Saves video without audio details in db
$db->query("INSERT INTO ".DB_PREFIX."videos (`token`,`pub`,`source`, `user_id`, `date`, `thumb`, `title`, `duration`, `tags` , `views` , `liked` , `category`, `description`, `nsfw`,`remote` ) VALUES 
('".$saveFile."','".intval(get_option('videos-initial'))."','localfile/".$saveFile.".mp4', '".$owner."', now() , '".get_option('mediafolder')."/thumbs/".$token.".jpg"."', '".toDb($video["description"]) ."', '6', '', '0', '0','".toDb($cat)."','".toDb($video["description"])."','0','')");

// Saves Audio from video into db
$db->query("INSERT INTO ".DB_PREFIX."videos (`media`,`token`,`pub`,`source`, `user_id`, `date`, `thumb`, `title`, `duration`, `tags` , `views` , `liked` , `category`, `description`, `nsfw`,`remote` ) VALUES 
('2','".$saveFile."','".intval(get_option('videos-initial'))."','localfile/".$saveFile.".aac', '".$owner."', now() , '".get_option('mediafolder')."/thumbs/xmp3.jpg"."', '".toDb($video["description"]) ."', '6', '', '0', '0','18','".toDb($video["description"])."','0','')");

}


/* SQL Insert 
$db->query("INSERT INTO ".DB_PREFIX."videos (`pub`,`source`, `user_id`, `date`, `thumb`, `title`, `duration`, `tags` , `views` , `liked` , `category`, `description`, `nsfw`,`remote` ) VALUES 
('".intval(get_option('videos-initial'))."','".$video["path"]."', '".$owner."', now() , '".$video["thumbnailUrl"]."', '".toDb($video["description"]) ."', '6', '', '0', '0','".toDb($cat)."','".toDb($video["description"])."','0','".$remote."')");	
*/

} else {
echo '<p><span class="redText">Missing important details </span></p>';
}
}
Class Vine {

    private static $username;
    private static $password;

    private $_baseURL = 'https://api.vineapp.com';

    public function __construct() {

        if (func_get_args(0))
            self::$username = func_get_arg(0);

        if (func_get_arg(1))
            self::$password = func_get_arg(1);
    }

    private function _getCurl($params = array()) {

    	$url = $params["url"];
    	$key = $params["key"];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "com.vine.iphone/1.0.3 (unknown, iPhone OS 6.1.0, iPhone, Scale/2.000000)");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('vine-session-id: '.$key));
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($ch);
		curl_close($ch);
		if (!$result){echo curl_error($ch);}

		return $result;

    }
    private function _postCurl($params = array()) {

		$url = $params["url"];
		$postFields = $params["postFields"];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		curl_setopt($ch, CURLOPT_USERAGENT, "com.vine.iphone/1.0.3 (unknown, iPhone OS 6.1.0, iPhone, Scale/2.000000)");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($ch);
		curl_close($ch);
		if (!$result){echo curl_error($ch);}

		return $result;
    }

    public function getKey() {

    	$username = urlencode(self::$username);
		$password = urlencode(self::$password);
		$postFields = "username=$username&password=$password"; 
		$url = $this->_baseURL.'/users/authenticate';
		$params = array("url" => $url, "postFields" =>$postFields,);
		$result = $this->_postCurl($params);
		$json = json_decode($result, true);
		$key = $json["data"]["key"];

		return $key;
    }

    public function getVinesbyTagJSON($tag,$page = null) {
	if(is_null($page)) {$page = 1;}

    	$key = $this->getKey();
    	$url = $this->_baseURL.'/timelines/tags/'.$tag.'?page='.$page.'&size=25';
		$params = array("url" => $url, "key" =>$key,);
		$result = $this->_getCurl($params);
		$json= json_decode($result, true);

		return $this->beautifyvine($json); 
    }
 public function getVinesbyPopular($page = null) {
	if(is_null($page)) {$page = 1;}

    	$key = $this->getKey();
    	$url = $this->_baseURL.'/timelines/popular?page='.$page.'&size=25';
		$params = array("url" => $url, "key" =>$key,);
		$result = $this->_getCurl($params);
		$json= json_decode($result, true);

		return $this->beautifyvine($json); 
    }
	
	 public function getVinesbyUser($userid, $page = null) {
	if(is_null($page)) {$page = 1;}
      if(!is_null($userid)) {
    	$key = $this->getKey();
    	$url = $this->_baseURL.'/timelines/users/'.$userid.'?page='.$page.'&size=25';
		$params = array("url" => $url, "key" =>$key,);
		$result = $this->_getCurl($params);
		$json= json_decode($result, true);

		return $this->beautifyvine($json); 
       } 	
    }
    public function getVinePostJSON($postId) {

    	$key = $this->getKey();
    	$url = $this->_baseURL.'/timelines/posts/'.$postId;
		$params = array("url" => $url, "key" =>$key,);
		$result = $this->_getCurl($params);
		$json= json_decode($result, true);

		return $json; 
    }

    public function beautifyvine($videos) {
	$toreturn = array();
	
	$toreturn['success'] = $videos["success"];
	if ($videos != null) {
	if ($videos["success"]) {
	$toreturn['total'] = $videos["data"]["count"];
	
			foreach ($videos as $vidz) {
			if(isset($vidz ["records"])) {
			foreach ($vidz ["records"] as $video) {
			if(isset($video) && !is_null($video) && !empty($video["permalinkUrl"]) && !is_null($video["thumbnailUrl"]) && !empty($video["thumbnailUrl"])) {
				$toreturn["videos"][] = (object) array(
					'avatarUrl'       => $video["avatarUrl"],
					'created'         => $video["created"],
					'description'     => $video["description"],
					'permalinkUrl'    => $video["permalinkUrl"],
					'postId'          => $video["postId"],
					'thumbnailUrl'    => $video["thumbnailUrl"],
					'userId'          => $video["userId"],
					'username'        => $video["username"],
					'explicitContent' => $video["explicitContent"],
					'verified'        => $video["verified"],
					'videoLowURL'     => $video["videoLowURL"],
					'videoUrl'        => $video["videoUrl"],
				);
			
			}
			}
			}
			}
	 $toreturn['returned'] = isset($toreturn["videos"])? count($toreturn["videos"]) : 0;						
	} else {
	$toreturn["error"] = $videos["error"];
	}
	}
   
	return $toreturn;
	}

}
function vineobj($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = vineobj($value);
        }
        return $result;
    }
    return $data;
}
?>
