<?php include_once('vine.class.php');
$vine = new Vine(get_option('vineusr'), get_option('vinepass'));
$importer = array_merge($_GET, $_POST);
$importer['allowduplicates'] = false;
?>
<?php if (isset($importer['action'])) {

switch ($importer['action']) {
case 'tag':
$vines = $vine->getVinesbyTagJSON($importer['key'], this_page());
$pagi_url = admin_url("vine").'&action=tag&key='.$importer['key'].'&categ='.$importer['categ'].'&owner='.$importer['owner'];
$pagi_url .= '&auto='.$importer['auto'].'&allowduplicates='.$importer['allowduplicates'].'&sleeppush='.$importer['sleeppush'].'&sleepvideos='.$importer['sleepvideos'].'&endpage='.$importer['endpage'].'&p=';
break;
default:
$vines = $vine->getVinesbyPopular(this_page());
$pagi_url = admin_url("vine").'&action=popular&categ='.$importer['categ'].'&owner='.$importer['owner'];
$pagi_url .= '&auto='.$importer['auto'].'&allowduplicates='.$importer['allowduplicates'].'&sleeppush='.$importer['sleeppush'].'&sleepvideos='.$importer['sleepvideos'].'&endpage='.$importer['endpage'].'&p=';
break;
}
if($vines["success"]) {
$nbTotal = isset($vines["total"]) ? $vines["total"] : 0;
//$nbTotal = $nbTotal - 1; /* Fix for the extra blank page */
$nb_display = 25;
if(isset($vines) && ($nbTotal > 0) && isset($vines["returned"]) && ($vines["returned"] > 0)) {

$a = new pagination;	
$a->set_current(this_page());
$a->set_first_page(true);
$a->set_pages_items(12);
$a->set_per_page($nb_display);
$a->set_values($nbTotal);
$a->show_pages($pagi_url);

//Owner recheck 
if(!isset($importer['owner']) || nullval($importer['owner'])) {$importer['owner'] = user_id();}

?>

<div class="row-fluid" style="padding: 10px 0">
</div>
<form id="validate" class="form-horizontal styled" action="<?php echo $pagi_url.this_page();?>" enctype="multipart/form-data" method="post">
<input type="hidden" name="categ" class="hide" value="<?php echo $importer['categ']; ?>"> 
<div class="table-overflow top10">
                        <table class="table table-bordered table-checks">
                          <thead>
                                <tr> 
                                                               
                                  <th width="130px">Picture</th>								 
                                  <th width="220px">Vine</th>
								  <th>NSFW</th>
								   <th>Created</th>
							      <th>Status</th>								  
                             <th>See it on Vine.co</th>
								</tr>
                          </thead>
                          <tbody>
						  <?php foreach ($vines["videos"] as $vine) { 
						  						  
						  ?>
                              <tr>
                                  <td><img src="<?php echo $vine->thumbnailUrl; ?>" style="width:130px; height:90px;"></td>
                                  <td ><?php echo _html($vine->description); ?></td>
								  <td>
								  <?php
                                  
								  echo intval($vine->explicitContent) ;?>
								  </td>
								  <td>
								  <?php
                                  $dt =explode('T',$vine->created);
								  $ds = explode('.',$dt[1]);
								  $df = $dt[0]. ''.$ds[0];
								  echo time_ago($df) ;?>
								  </td>
                                  <td>
								    <?php 
									 if($importer['allowduplicates'] > 0) {
									 echo '<span class="greenText">Imported</span>';
								  vine_import(vineobj($vine),$importer['categ'],$importer['owner'] );
									 } else {
									if(has_vine_duplicate($vine->permalinkUrl)) {
								    echo '<span class="redText">Warning: Already saved.</span>';
								   } else {
								   echo '<span class="greenText">Imported</span>';
								 vine_import(vineobj($vine),$importer['categ'],$importer['owner'] );							
								   }
                                   }
                                   ?>
								  </td>
								  <td><a class="btn btn-primary" href="<?php echo $vine->permalinkUrl; ?>" target="_blank">@Vine</a></td>
                                  
                              </tr>
							  <?php 
							if($importer['sleepvideos'] > 0) {   sleep($importer['sleepvideos']); }
							  }  //end loop 
							  ?>
						</tbody>  
</table>
</form>
</div>						
<?php
$next = this_page() + 1;
if(($importer['auto'] > 0) && ($nbTotal > 0) && ($next < $importer['endpage'])) {
echo 'Redirecting to '.$next;
echo '
<script type="text/javascript">
setTimeout(function() {
  window.location.href = "'.$pagi_url.$next.'";
}, '.$importer['sleeppush'].');

</script>
';
}
$a->show_pages($pagi_url);
} else {
$error = isset($vines["error"]) ? $vines["error"] : "No more results from Vine.co";
echo '<div class="msg-warning">'.$error.'</div>';
}
} else {
echo '<div class="msg-warning">Something went wrong</div>';
}


} ?>
<?php if (!isset($importer['action'])) { ?>
<h2 class=""> Vine.co importer</h2>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#search">Import by #Tag</a></li>
  <li><a href="#popular">Popular Vines</a></li>
   
</ul>

<div class="tab-content" style="min-height:900px">
  <div class="tab-pane active" id="search">
  <div class="row-fluid">
<form id="validate" class="form-horizontal styled" action="<?php echo admin_url('vine');?>" enctype="multipart/form-data" method="post">

<input type="hidden" name="action" class="hide" value="tag"> 
<div class="control-group">
<label class="control-label"><i class="icon-search"></i>Tag</label>
<div class="controls">
<input type="text" name="key" class="validate[required] span8" value=""> 						
</div>	
</div>

<?php
echo '<div class="control-group">
	<label class="control-label">'._lang("Category:").'</label>
	<div class="controls">
	'.cats_select("categ","select","").'
	  </div>             
	  </div>';
?>	  
	<div class="control-group">
	<label class="control-label">User</label>
	<div class="controls">
	<?php
	echo '<select data-placeholder="'._lang("Choose owner:").'" name="owner" id="clear-results" class="select validate[required]" tabindex="2">
	';
$users = $db->get_results("SELECT id, name FROM  ".DB_PREFIX."users order by id asc limit 0,1000");
if($users) {
foreach ($users as $cat) {	
echo'<option value="'.intval($cat->id).'">'.stripslashes($cat->name).'</option>';
	}
}	else {
echo'<option value="">'._lang("No users").'</option>';
}
echo '</select>';
	
	?>
	</div>
	</div>
<div class="control-group">
	<label class="control-label">Autopush</label>
	<div class="controls">
	<label class="radio inline"><input type="radio" name="auto" class="styled" value="1"> YES </label>
	<label class="radio inline"><input type="radio" name="auto" class="styled" value="0" checked>NO</label>
	</div>
	</div>	
	<div class="control-group">
	<label class="control-label">Allow duplicates</label>
	<div class="controls">
	<label class="radio inline"><input type="radio" name="allowduplicates" class="styled" value="1"> YES </label>
	<label class="radio inline"><input type="radio" name="allowduplicates" class="styled" value="0" checked>NO</label>
	<span class="help-block">If set to NO it will search if video is already in the database and skip it. </span>				
		
	</div>
	</div>	
	<div class="control-group">
	<label class="control-label">Advanced settings</label>
	<div class="controls">
<div class="row-fluid">
	<div class="span4">
		<input class="span12" name="sleeppush" type="text" value="2"><span class="help-block">Seconds to sleep before push </span>
	</div>
	<div class="span4">
		<input class="span12" name="sleepvideos" type="text" value="0"><span class="help-block k align-center">Seconds to sleep between videos import</span>
	</div>
	<div class="span4">
		<input class="span12" name="endpage" type="text" value="19"><span class="help-block k align-right">Which page to end push  </span>
	</div>
</div>
	</div>
	</div>	
		
<div class="control-group">
<button type="submit" class="pull-right btn btn-success">Start import</button> 						

</div>	  
	</form>    
    </div>
   </div> 
  <div class="tab-pane" id="popular">
  <div class="row-fluid">
<form id="validate" class="form-horizontal styled" action="<?php echo admin_url('vine');?>" enctype="multipart/form-data" method="post">

<input type="hidden" name="action" class="hide" value="popular"> 

<?php
echo '<div class="control-group">
	<label class="control-label">'._lang("Category:").'</label>
	<div class="controls">
	'.cats_select("categ","select","").'
	  </div>             
	  </div>';
?>	  
	<div class="control-group">
	<label class="control-label">User</label>
	<div class="controls">
	<?php
	echo '<select data-placeholder="'._lang("Choose owner:").'" name="owner" id="clear-results" class="select validate[required]" tabindex="2">
	';
$users = $db->get_results("SELECT id, name FROM  ".DB_PREFIX."users order by id asc limit 0,1000");
if($users) {
foreach ($users as $cat) {	
echo'<option value="'.intval($cat->id).'">'.stripslashes($cat->name).'</option>';
	}
}	else {
echo'<option value="">'._lang("No users").'</option>';
}
echo '</select>';
	
	?>
	</div>
	</div>

	<div class="control-group">
	<label class="control-label">Autopush</label>
	<div class="controls">
	<label class="radio inline"><input type="radio" name="auto" class="styled" value="1"> YES </label>
	<label class="radio inline"><input type="radio" name="auto" class="styled" value="0" checked>NO</label>
	</div>
	</div>	
	<div class="control-group">
	<label class="control-label">Allow duplicates</label>
	<div class="controls">
	<label class="radio inline"><input type="radio" name="allowduplicates" class="styled" value="1"> YES </label>
	<label class="radio inline"><input type="radio" name="allowduplicates" class="styled" value="0" checked>NO</label>
	<span class="help-block">If set to NO it will search if video is already in the database and skip it. </span>				
		
	</div>
	</div>	
<div class="control-group">
	<label class="control-label">Advanced settings</label>
	<div class="controls">
<div class="row-fluid">
	<div class="span4">
		<input class="span12" name="sleeppush" type="text" value="2"><span class="help-block">Seconds to sleep before push </span>
	</div>
	<div class="span4">
		<input class="span12" name="sleepvideos" type="text" value="0"><span class="help-block k align-center">Seconds to sleep between videos import</span>
	</div>
	<div class="span4">
		<input class="span12" name="endpage" type="text" value="19"><span class="help-block k align-right">Which page to end push  </span>
	</div>
</div>
	</div>
	</div>		
<div class="control-group">
<button type="submit" class="pull-right btn btn-success">Start import</button> 						

</div>	  
	</form>    
    </div>
   </div> 
  </div> 
   
   <?php } ?>