<?php
if(isset($_POST['update_options_now'])){
foreach($_POST as $key=>$value)
{
update_option($key, $value);
}
  echo '<div class="msg-info">Vine options have been updated.</div>';
  $db->clean_cache();
}
$all_options = get_all_options();
?>
<div class="row-fluid">
<h3>Vine Settings</h3>
<form id="validate" class="form-horizontal styled" action="<?php echo admin_url('vinestts');?>" enctype="multipart/form-data" method="post">
<fieldset>
<input type="hidden" name="update_options_now" class="hide" value="1" /> 


	
	<div class="control-group">
<label class="control-label"><i class="icon-key"></i>Vine connect</label>
 <div class="controls">
<div class="row-fluid">
<div class="span6">
<input type="text" name="vineusr" class="span12" value="<?php echo get_option('vineusr'); ?>"><span class="help-block">Your Vine.co email </span>
</div>
<div class="span6">
<input type="password" name="vinepass" class="span12" value="<?php echo get_option('vinepass'); ?>"><span class="help-block align-center"> Your Vine.co password</span>
</div>

</div>
</div>
</div>
	<div class="control-group">
	<label class="control-label"><i class="icon-check"></i>Link to</label>
	<div class="controls">
	<label class="radio inline"><input type="radio" name="vinemode" class="styled" value="1" <?php if(get_option('vinemode') == 1 ) { echo "checked"; } ?>>Direct video link (mp4)</label>
	<label class="radio inline"><input type="radio" name="vinemode" class="styled" value="0" <?php if(get_option('vinemode') == 0 ) { echo "checked"; } ?>>Embed Vine post</label>

	</div>
	</div>
<div class="control-group">
<button class="btn btn-large btn-primary pull-right" type="submit"><?php echo _lang("Update settings"); ?></button>	
</div>	
</fieldset>						
</form>
</div>