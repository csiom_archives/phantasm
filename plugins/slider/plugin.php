<?php
/**
 * Plugin Name: Easy playlist-alike slider
 * Plugin URI: http://get.phpvibe.com/
 * Description: Adds an playlist designed-alike slider or videos (own iframe).
 * Version: 1.1
 * Author: PHPVibe Crew
 * Author URI: http://www.phpvibe.com
 * License: Commercial
 */
 //Start slider
function slider_local_check($url){
if (strpos($url,'localfile') !== false) {
	return true;	
	} else {
	return false;
	}
}
function _renderSlider($text){
global $db;
$options = DB_PREFIX."videos.id as vid,".DB_PREFIX."videos.title,".DB_PREFIX."videos.source,".DB_PREFIX."videos.user_id as owner, ".DB_PREFIX."videos.thumb,".DB_PREFIX."videos.views,".DB_PREFIX."videos.liked,".DB_PREFIX."videos.duration,".DB_PREFIX."videos.nsfw";
$query = get_option("slider-queries","recent") ;
$limit = get_option("slider-nr","25");
if($query == "most_viewed"):
$vq = "select ".$options.", ".DB_PREFIX."users.name  FROM ".DB_PREFIX."videos LEFT JOIN ".DB_PREFIX."users ON ".DB_PREFIX."videos.user_id = ".DB_PREFIX."users.id WHERE ".DB_PREFIX."videos.views > 0 and pub > 0 and media < 2 ORDER BY ".DB_PREFIX."videos.views DESC ".this_offset($limit);
elseif($query == "top_rated"):
$vq = "select ".$options.", ".DB_PREFIX."users.name  FROM ".DB_PREFIX."videos LEFT JOIN ".DB_PREFIX."users ON ".DB_PREFIX."videos.user_id = ".DB_PREFIX."users.id WHERE ".DB_PREFIX."videos.liked > 0 and pub > 0 and media < 2 ORDER BY ".DB_PREFIX."videos.liked DESC ".this_offset($limit);
elseif($query == "featured"):
$vq = "select ".$options.", ".DB_PREFIX."users.name  FROM ".DB_PREFIX."videos LEFT JOIN ".DB_PREFIX."users ON ".DB_PREFIX."videos.user_id = ".DB_PREFIX."users.id WHERE ".DB_PREFIX."videos.featured = '1' and pub > 0 and media < 2 ORDER BY ".DB_PREFIX."videos.id DESC ".this_offset($limit);
else:
$vq = "select ".$options.", ".DB_PREFIX."users.name  FROM ".DB_PREFIX."videos LEFT JOIN ".DB_PREFIX."users ON ".DB_PREFIX."videos.user_id = ".DB_PREFIX."users.id WHERE ".DB_PREFIX."videos.views >= 0 and pub > 0 and media < 2 ORDER BY ".DB_PREFIX."videos.id DESC ".this_offset($limit);
endif;
$result = $db->get_results($vq);
if ($result) {
echo '<div class="row-fluid block player-in-list ">
<div id="video-content" class="span77">
<div class="video-player pull-left " style="min-height: 505px;">
<iframe class="vibe_embed" width="100%" height="60%" src="" frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
</div>
</div>
<div id="ListRelated" class="video-under-right nomargin pull-right span44">
<div class="video-player-sidebar pull-left">
<div class="video-header row-fluid list-header">
<span class="tt">'._lang(get_option('slider-header', 'My awesome slider')).'</span>
</div>
<div class="items">
<ul>
';	
	
	foreach ($result as $related) {
		$nowP = ($related->vid == token_id())? "playingNow" : "";
$duration = ($related->duration > 0) ? video_time($related->duration) : '<i class="icon-picture"></i>';		
$local = (slider_local_check($related->source)) ? "slideLocal" : "";
echo '
					<li data-id="'.$related->vid.'" class="item-post '.$nowP.' '.$local.'">
				<div class="inner">
					
	<div class="thumb">
		<a class="clip-link" data-id="'.$related->vid.'" title="'._html($related->title).'" href="'.video_url($related->vid , $related->title).'">
			<span class="clip">
				<img src="'.thumb_fix($related->thumb).'" alt="'._html($related->title).'" /><span class="vertical-align"></span>
			</span>
		<span class="timer">'.$duration.'</span>					
			<span class="overlay"></span>
		</a>
	</div>			
					<div class="data">
						<span class="title"><a href="'.video_url($related->vid , $related->title).'" rel="bookmark" title="'._html($related->title).'">'._cut(_html($related->title),124 ).'</a></span>
			
						<span class="usermeta">
							'._lang('by').' <a href="'.profile_url($related->owner, $related->name).'"> '._html($related->name).' </a>
							
						</span>
					</div>
				</div>
				</li>
		
	';
	}
echo '</ul></div></div></div>
</div>
<script>
$(document).ready(function(){	
	function changeIframe(vid,local) {
	$(".vibe_embed").attr("src", "'.site_url().embedcode.'/"+ vid +"/")
	var ew = $(".video-player").width();
	if(local) {
    var eh = Math.round((ew/16)*9) + 20;
	} else {
	var eh = Math.round((ew/16)*9);	
	}
    $(".vibe_embed").height(eh); 	
	}	
	var firstSlide = $(".items li").first().attr("data-id");
	if($(".items li").first().hasClass( "slideLocal" )) {
	changeIframe(firstSlide,1);	
	} else {
	changeIframe(firstSlide);	
	}	
	$(".item-post a").click(function (e) {
		  e.preventDefault();		  
		  var idSlide = $(this).parents("li:first").attr("data-id");
		  var idTitle = $(this).attr("title");
		  $(".tt").html(idTitle);
		  if($(this).parents("li:first").hasClass( "slideLocal" )) {
          changeIframe(idSlide,1);		
		  } else {
			changeIframe(idSlide);  
		  }
		});
 });	
</script>
<br style="clear:both">';
	
	}
//End slider
}
function sliderlink($txt = '') {
return $txt.'
<li><a href="'.admin_url('slider').'"><i class="icon-spinner"></i>Slider</a></li>
';
}
function _adminSlider() {
global $db,$all_options;	
if(isset($_POST['update_options_now'])){
foreach($_POST as $key=>$value)
{
  update_option($key, $value);
}
$db->clean_cache();
 echo '<div class="msg-info">Slider is updated.</div>';	
 $all_options = get_all_options();
}
 
echo '<h3>Slider Settings</h3>
<form id="validate" class="form-horizontal styled" action="'.admin_url("slider").'" enctype="multipart/form-data" method="post">
<fieldset>
<input type="hidden" name="update_options_now" class="hide" value="1" /> 	
<div class="control-group">
	<label class="control-label">Title</label>
	<div class="controls">
	<input type="text" id="slider-header" name="slider-header" class="span6" value="'.get_option("slider-header","My awesome slider").'">
	<span class="help-block" id="limit-text">Slider header <em>(translatable trough languages)</em>.</span>

	</div>
	</div>	
	<div class="control-group">
	<label class="control-label">Number of videos</label>
	<div class="controls">
	<input type="text" id="slider-nr" name="slider-nr" class="span6" value="'.get_option("slider-nr","25").'">
	<span class="help-block" id="limit-text">Video query limit.</span>
	</div>
	</div>	
		<div class="control-group">
	<label class="control-label">Video query:</label>
	<div class="controls">
	<select data-placeholder="Select type" name="slider-queries" id="slider-queries" class="select validate[required]" tabindex="2">
	<option value="most_viewed">Most viewed </option>
<option value="top_rated">Most Liked</option>
<option value="recent" >Recent</option>
<option value="featured">Featured</option>
	</select>
	</div>
	</div>	
	<script>
	      $(document).ready(function(){
	$(\'.select\').find(\'option[value="'.get_option("slider-queries","recent").'"]\').attr("selected",true);	
});
	</script>

	<div class="control-group">
<button class="btn btn-large btn-primary pull-right" type="submit">Update settings</button>	
</div>
</fieldset>	
</form>		
';	
}
add_filter('configuration_menu', 'sliderlink');
add_action('home-start', '_renderSlider');
add_action('adm-slider', '_adminSlider');
?>