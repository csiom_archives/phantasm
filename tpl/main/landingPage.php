<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Phantasm</title>

<!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<style>video{top:0;left:0;width:100%!important;height:100%!important;object-fit:cover}#bg-video{background: url(image.jpg)  no-repeat scroll center top / 100% auto;height:100%;position:absolute;width:100%;z-index:-1}body{background-image:url(image.jpg);background-position:center center;background-repeat:no-repeat;background-attachment:fixed;background-size:cover;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif}.a{text-align:center;color:#fff;margin:200px 0 80px;font-size:62px}.b{text-align:center;color:#000;margin:0 0 60px;font-size:20px}.c{text-align:center;color:#fff;margin:260px 0}.b1{ width:15%;background:#000;font-size:24px;padding:10px 30px;margin:0px 30px;border:2px solid #000;border-radius:4px;color:#fff;transition:all .4s ease .1s}.b1:hover{color:#fff;transform:scale(1.1)} .b2{ width:15%;background:#000;font-size:24px;padding:10px 30px;margin:0px 30px;border:2px solid #000;border-radius:4px;color:#fff;transition:all .4s ease .1s} .b2:hover{color:#fff;transform:scale(1.1);} .h{width:50%;margin:0 auto;color:#f4f4f4;height:.5px;position:relative}@media (min-width:200px) and (max-width:400px){video{visibility:hidden;background: url(image.jpg)  no-repeat scroll center top / 100% auto}#video_div{display:none!important;height:730px}.container{width:100%;height:30%}.a{text-align:center;color:#fff;margin:150px 0 30px;font-size:28px;width:100%}.b{text-align:center;color:#000;margin:0 0 50px;font-size:18px;width:100%}.c{text-align:center;color:#fff;margin:260px 0;width:100%}.h{display:none}.b1{width:50%;margin:5px auto;bottom:5px}.b2{width:50%;margin:5px auto}}@media (min-width:401px) and (max-width:900px){.container{width:100%;height:30%}.a{text-align:center;color:#fff;margin:180px 0 50px;font-size:46px}.b{text-align:center;color:#000;margin:0 0 50px;font-size:16px}.c{text-align:center;color:#fff;margin:260px 0}.h{width:400px}.b1{width:15%;margin:5px 20px}.b2{width:15%;margin:5px 20px}}@media (min-width:401px) and (max-width:900px) and (max-height:600px){video{visibility:hidden;background: url(image.jpg)  no-repeat scroll center top / 100% auto}#video_div{display:none!important;height:730px}.container{width:100%;height:30%;margin-top:-25%}.a{text-align:center;color:#fff;margin:150px 0 30px;font-size:28px;width:100%}.b{text-align:center;color:#000;margin:0 0 50px;font-size:18px;width:100%}.c{text-align:center;color:#fff;margin:260px 0;width:100%}.h{display:none}.b1{width:50%;margin:5px auto;bottom:5px}.b2{width:50%;margin:5px auto}}@media (max-height:320px){.container{width:100%;height:30%;margin-top:-38%}}@media (max-width:320px){.container{width:100%;height:30%;margin-top:-46%}}</style>
<body>
<?php include_once("analyticstracking.php") ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63846017-1', 'auto');
  ga('send', 'pageview');

</script>



<!-- add this part in header -->
<video autoplay loop="loop" id="bg-video">
    <!--add here your video file in mp4-->
    <source src="video.mp4" type="video/mp4">
    <!-- in other formates-->
    <source src="video.webm" type="video/webm">
</video>

<div class="container">

<div class="row">

<center>
        <div class="col-sm-12 c">
        <a href="<?php echo site_url().'register/'; ?>"><button class="b2">Register</button></a>
        <a href="<?php echo site_url().'login/'; ?>"><button class=" b1">Login</button></a>
        </div>
</center>
</div>
</div>

</body>
</html>
